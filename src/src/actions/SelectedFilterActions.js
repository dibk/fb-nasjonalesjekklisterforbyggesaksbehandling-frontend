import {
  ADD_SELECTEDFILTER,
  REMOVE_SELECTEDFILTER,
  UPDATE_SELECTEDFILTER
} from 'constants/types';

const getFilterParents = (filter, filterParents = []) => {
  if (filter.parent){
    filterParents.push(filter.parent.id);
    getFilterParents(filter);
  }
  return filterParents;
}

export const addSelectedFilter = filter => (dispatch, getState) => {
  const filterParents = getFilterParents(filter);
  const selectedFilters = getState().selectedFilters;
  if (filterParents && filterParents.length){
    filterParents.forEach(filterParent => {

    })
  }
  dispatch({
    type: ADD_SELECTEDFILTER,
    payload: filter
  })
};

export const removeSelectedFilter = filter => (dispatch, getState) => {
  const filterParents = [];
  if (filter.parent){
    filterParents.push(filter.parent.id)
  }
  dispatch({
    type: REMOVE_SELECTEDFILTER,
    payload: filter
  })
};

export const updateSelectedFilter = filter => dispatch => {
  dispatch({
    type: UPDATE_SELECTEDFILTER,
    payload: filter
  })
};
