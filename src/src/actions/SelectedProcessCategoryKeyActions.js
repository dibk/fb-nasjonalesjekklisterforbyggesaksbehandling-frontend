import { UPDATE_SELECTEDPROCESSCATEGORYKEY } from 'constants/types';

export const updateSelectedProcessCategoryKey = processCategoryKey => dispatch => {
  dispatch({
    type: UPDATE_SELECTEDPROCESSCATEGORYKEY,
    payload: processCategoryKey
  })
};
