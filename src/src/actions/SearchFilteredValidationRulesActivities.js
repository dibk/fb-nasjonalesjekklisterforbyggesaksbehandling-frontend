// Types
import { UPDATE_SEARCH_FILTERED_VALIDATION_RULES_ACTIVITIES } from "constants/types";

export const updateSearchFilteredValidationRulesActivities = (searchFilteredValidationRulesActivities) => (dispatch) => {
    return dispatch({
        type: UPDATE_SEARCH_FILTERED_VALIDATION_RULES_ACTIVITIES,
        payload: searchFilteredValidationRulesActivities
    });
};
