import { FETCH_ACTIVITY } from 'constants/types';

const initialState = [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ACTIVITY:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
