import { FETCH_ACTIVITY_MODEL } from 'constants/types';

const initialState = {};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ACTIVITY_MODEL:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
