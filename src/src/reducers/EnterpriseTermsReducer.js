import { FETCH_ENTERPRISETERMS } from 'constants/types';

const initialState = {};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ENTERPRISETERMS:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
