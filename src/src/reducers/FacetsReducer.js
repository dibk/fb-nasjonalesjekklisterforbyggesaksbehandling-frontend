import { FETCH_FACETS } from 'constants/types';

const initialState = [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FACETS:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
