import { FETCH_PROCESSCATEGORIES } from 'constants/types';

const initialState = [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PROCESSCATEGORIES:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
