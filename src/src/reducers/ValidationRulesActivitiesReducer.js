import { FETCH_VALIDATION_RULES_ACTIVITIES } from "constants/types";

const initialState = [];

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_VALIDATION_RULES_ACTIVITIES:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;
