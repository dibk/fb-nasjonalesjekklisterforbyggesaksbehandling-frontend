import { FETCH_ACTIONS } from 'constants/types';

const initialState = [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ACTIONS:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
