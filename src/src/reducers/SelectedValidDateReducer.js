import { UPDATE_SELECTED_VALID_DATE_FROM_URL } from 'constants/types';

const initialState = null;

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_SELECTED_VALID_DATE_FROM_URL:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
