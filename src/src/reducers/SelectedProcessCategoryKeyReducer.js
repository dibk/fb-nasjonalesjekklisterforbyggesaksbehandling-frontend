import { UPDATE_SELECTEDPROCESSCATEGORYKEY } from 'constants/types';

const initialState = {};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_SELECTEDPROCESSCATEGORYKEY:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
