import { FETCH_METADATATYPES } from 'constants/types';

const initialState = [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_METADATATYPES:
      return action.payload;
    default:
      return state;
  }
};

export default reducer;
