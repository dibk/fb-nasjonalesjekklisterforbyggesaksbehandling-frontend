export const isValidSelectedOptionValue = (options, value) => {
    return options.some(options => {
        return options.value === value
    });
}
