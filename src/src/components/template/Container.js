// Dependencies
import React from 'react';

// Stylesheets
import style from 'components/template/Container.module.scss';

class Container extends React.Component {
  render() {
    return (<div className={style.container}>
      {this.props.children}
    </div>)
  }
};

export default Container;
