// Dependencies
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

// DIBK Design
import {CheckBoxInput, ListItem} from 'dibk-design';

// Actions
import {addSelectedEnterpriseTerm, removeSelectedEnterpriseTerm} from '../../../actions/SelectedEnterpriseTermsActions';

// Stylesheets
class EnterpriseTerm extends Component {

  enterpriseTermCheckboxOnChange(enterpriseTermCode, isChecked) {
    isChecked
      ? this.props.removeSelectedEnterpriseTerm(enterpriseTermCode)
      : this.props.addSelectedEnterpriseTerm(enterpriseTermCode);
    this.setState({})
  }

  isEnterpriseTermChecked(enterpriseTermCode, selectedEnterpriseTerms) {
    if (selectedEnterpriseTerms && selectedEnterpriseTerms.length) {
      return selectedEnterpriseTerms.find(selectedEnterpriseTerm => {
        return selectedEnterpriseTerm.code === enterpriseTermCode
      })
        ? true
        : false;
    } else {
      return false;
    }
  }

  render() {
    if (this.props.enterpriseTermName && this.props.enterpriseTermCode && this.props.selectedEnterpriseTerms) {
      const isChecked = this.isEnterpriseTermChecked(this.props.enterpriseTermCode, this.props.selectedEnterpriseTerms);
      if (this.props.editable) {
        return (
          <ListItem key={this.props.enterpriseTermCode}>
            <CheckBoxInput id={this.props.enterpriseTermCode} checked={isChecked} onChange={() => this.enterpriseTermCheckboxOnChange(this.props.enterpriseTermCode, isChecked)}>
              {this.props.enterpriseTermName}
            </CheckBoxInput>
        </ListItem>
        )
      } else if (isChecked) {
        return <ListItem key={this.props.enterpriseTermCode}>{this.props.enterpriseTermName}</ListItem>
      } else {
        return null
      }
    }
  }
}

EnterpriseTerm.propTypes = {
  enterpriseTermName: PropTypes.string,
  enterpriseTermCode: PropTypes.string.isRequired,
  editable: PropTypes.bool
};

EnterpriseTerm.defaultProps = {
  editable: false
}

const mapStateToProps = state => ({enterpriseTerms: state.enterpriseTerms, selectedEnterpriseTerms: state.selectedEnterpriseTerms});

const mapDispatchToProps = {
  addSelectedEnterpriseTerm,
  removeSelectedEnterpriseTerm
};

export default connect(mapStateToProps, mapDispatchToProps)(EnterpriseTerm);
