// Dependencies
import React, { Fragment } from "react";
import { Link } from "react-router-dom";

// DIBK Design
import { Header, Button, Lead } from "dibk-design";

// Helpers
import { classNameArrayToClassNameString } from "helpers/guiHelpers";

// Stylesheets
import wizardStyle from "components/routes/Wizard.module.scss";

const PlanHomeContent = ({ headerContent, checklistApiUrl }) => {
    return (
        <Fragment>
            <Header label="Veiviser" size={1}>
                {headerContent}
            </Header>
            <Header content="Slik bruker du sjekklistene" size={2} />
            <p>
                Sjekklistene gir deg som plansaksbehandler en komplett liste over hvilke punkter kommunen skal vurdere i
                plansaker.
            </p>
            <p>Du får også en forklaring på hvordan vurderingen skal gjøres.</p>
            <p>
                Underlaget for dette arbeidet har blant annet vært KS sin kravspesifikasjon for ePlansak, KDD sine
                veiledere og sjekklister fra en rekke kommuner.
            </p>
            <div className={classNameArrayToClassNameString([wizardStyle.buttonRow, wizardStyle.marginBottom])}>
                <Button type="button" color="primary">
                    <Link to="/checklist">Velg sjekkliste</Link>
                </Button>
            </div>
            <Lead>
                Sjekklistene kan også brukes via vårt API.
                <a href={checklistApiUrl}>Api for sjekklistene</a>
            </Lead>
        </Fragment>
    );
};

export default PlanHomeContent;
